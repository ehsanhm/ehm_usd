from pxr import Usd, Sdf, UsdGeom


def main():
    stage = Usd.Stage.CreateNew("path.usda")

    cube_path = Sdf.Path("/World/Xform/Cube")
    stage.DefinePrim(cube_path)
    property_path = cube_path.AppendProperty(UsdGeom.Tokens.points)
    target_path = property_path.AppendTarget("target1")


    sphere_path = Sdf.Path('/World/Parent/Sphere')
    stage.DefinePrim(sphere_path)

    print(cube_path.pathString)
    print(cube_path.GetParentPath())
    print(cube_path.name)

    print(property_path)
    print(target_path)

    print(cube_path.GetCommonPrefix(sphere_path))

    print(cube_path.MakeRelativePath(sphere_path))

    cube_new_path = cube_path.ReplaceName("aaa")
    print(cube_new_path.pathString)

    stage.Save()


if __name__ == "__main__":
    main()
