from pxr import Usd, UsdGeom


def main():
    # create an xform and a sphere under it
    stage = Usd.Stage.CreateNew("sphere_under_xform.usda")

    xform = UsdGeom.Xform.Define(stage, "/Xform")
    UsdGeom.XformCommonAPI(xform).SetTranslate([10, 0, 0])
    UsdGeom.XformCommonAPI(xform).SetRotate([90, 0, 0])

    sphere = UsdGeom.Sphere.Define(stage, "/Xform/Sphere")
    sphere.GetRadiusAttr().Set(2)

    stage.SetDefaultPrim(xform.GetPrim())

    stage.Save()

    # reference the file we just created
    stage = Usd.Stage.CreateNew("reference.usda")

    ref = stage.OverridePrim("/Ref")
    ref.GetReferences().AddReference("./sphere_under_xform.usda")

    ref2 = UsdGeom.Xform.Define(stage, "/Ref2")
    ref2.SetXformOpOrder([])
    ref2.GetPrim().GetReferences().AddReference("sphere_under_xform.usda")

    sphere = UsdGeom.Sphere.Get(stage, "/Ref/Sphere")
    sphere.GetDisplayColorAttr().Set([(1, 0, 0)])

    stage.Save()
    stage.Export("reference_flattened.usda")
    # print(stage.GetRootLayer().ExportToString())


if __name__ == "__main__":
    main()
