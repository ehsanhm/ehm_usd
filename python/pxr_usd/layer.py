from pxr import Usd, UsdGeom, Sdf


def main():
    # create big_ball.usda
    ball_stage = Usd.Stage.CreateNew("big_ball.usda")
    ball_stage.SetStartTimeCode(1)
    ball_stage.SetEndTimeCode(24)
    big_ball = UsdGeom.Sphere.Define(ball_stage, "/Big_Ball")
    radius_a = big_ball.CreateRadiusAttr()
    radius_a.Set(2.0)
    radius_a.Set(1.0, 1)
    radius_a.Set(3.0, 24)
    ball_stage.Save()

    # create small_box.usda
    box_stage = Usd.Stage.CreateNew("small_box.usda")
    UsdGeom.Cube.Define(box_stage, "/Small_Box")
    box_stage.Save()

    # create box_and_ball.usda
    assembly_layer = Sdf.Layer.CreateNew("box_and_ball.usda")
    assembly_layer.startTimeCode = 1
    assembly_layer.endTimeCode = 24

    # add a layer (using its full path)
    assembly_layer.subLayerPaths.append(box_stage.GetRootLayer().identifier)

    # add a layer (using just its name == relative path)
    assembly_layer.subLayerPaths.append(ball_stage.GetRootLayer().GetDisplayName())

    assembly_layer.Save()


if __name__ == "__main__":
    main()
