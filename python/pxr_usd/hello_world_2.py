from pxr import Usd, UsdGeom


def main():
    stage = Usd.Stage.CreateNew("hello_world_2.usda")

    stage.DefinePrim("/Hello", "Xform")
    stage.DefinePrim("/Hello/World", "Sphere")

    stage.Save()


if __name__ == "__main__":
    main()
