from pxr import Usd, UsdGeom


def main():
    stage = Usd.Stage.CreateInMemory()

    planet = UsdGeom.Sphere.Define(stage, "/World/Planet")

    radius_a = planet.CreateRadiusAttr()
    # if we set the radius, variantSets opinion on radius will be ignored
    # radius_a.Set(1.0)

    # get access to prim's variant sets
    # think of this as a tool that can create variant sets
    planet_p = planet.GetPrim()
    vsets = planet_p.GetVariantSets()

    # add a variant set
    # think of this as a menu that holds different options
    planet_name_vset = vsets.AddVariantSet("PlanetName")

    # add variants
    # think of this as adding different items to the options menu
    planet_name_vset.AddVariant("Mars")
    planet_name_vset.AddVariant("Earth")
    planet_name_vset.AddVariant("Jupiter")

    # connect the added options to radius, so that if user 
    # chooses Jupiter, the planet should get very big
    planet_name_vset.SetVariantSelection("Jupiter")
    with planet_name_vset.GetVariantEditContext():
        radius_a.Set(2.0)

    planet_name_vset.SetVariantSelection("Earth")
    with planet_name_vset.GetVariantEditContext():
        radius_a.Set(1.0)
    
    planet_name_vset.SetVariantSelection("Mars")
    with planet_name_vset.GetVariantEditContext():
        radius_a.Set(0.5)

    # We can find out which variant is set currently
    print(planet_name_vset.GetVariantSelection())

    # clear variant selection in case we don't want any local opinion for the variant
    planet_name_vset.ClearVariantSelection()

    # show resulting USD layer content
    print(stage.GetRootLayer().ExportToString())

    # show resulting composed stage content
    print(stage.ExportToString())

    # save 
    stage.GetRootLayer().Export("variants.usda")


if __name__ == "__main__":
    main()
