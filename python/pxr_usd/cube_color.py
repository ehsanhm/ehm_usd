from pxr import Usd, UsdGeom, Gf


def main():
    stage = Usd.Stage.CreateInMemory()

    cube_s = UsdGeom.Cube.Define(stage, "/Cube")

    color_a = cube_s.GetDisplayColorAttr()
    color_a.Set([Gf.Vec3f(1, 0, 0)])

    size_a = cube_s.GetSizeAttr()
    size_a.Set(1.42)

    print(stage.GetRootLayer().ExportToString())

    cube_p = cube_s.GetPrim()
    cube_p.SetPropertyOrder(["size", "primvars:displayColor"])

    stage.Export("cube_color.usda")


if __name__ == "__main__":
    main()
