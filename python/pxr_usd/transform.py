from pxr import Usd, UsdGeom, Gf


def main():
    stage = Usd.Stage.CreateInMemory()

    parent = UsdGeom.Xform.Define(stage, "/Parent")
    cube = UsdGeom.Cube.Define(stage, "/Parent/MyCube")
    
    # move them
    set_local_transform(parent, Gf.Matrix4d((1, 0, 0, 0), (0, 1, 0, 0), (0, 0, 1, 0), (100, 0, 0, 1)))
    # set_local_transform(cube, Gf.Matrix4d((1, 0, 0, 0), (0, 1, 0, 0), (0, 0, 1, 0), (0, 50, 0, 1)))
    set_TRS(
        cube,
        translate=[0, 50, 0],
        rotate=[45, 0, 0],
        scale=[2, 1, 1]
    )

    # bbox: slow
    cube_bound = get_bbox(cube)
    print(cube_bound.GetBox())

    # bbox: faster if query bbox value multiple times from cache 
    purposes =[UsdGeom.Tokens.default_, UsdGeom.Tokens.proxy, UsdGeom.Tokens.render]
    bbox_cache = UsdGeom.BBoxCache(0, purposes)
    cube_bound = get_bbox_from_cache(bbox_cache=bbox_cache, prim=cube.GetPrim())
    print(cube_bound.GetBox())

    # local transform
    local_mtx = get_local_transform(cube)
    print(f"Local Matrix: {local_mtx}")

    # world transform
    world_mtx = get_world_transform(cube)
    print(f"World Matrix: {world_mtx}")

    # parent matrix
    parent_mtx = get_parent_matrix(cube)
    print(f"Parent Matrix: {parent_mtx}")

    # get TRS
    trs = extract_trs(world_mtx)
    print(f"TRS: {trs}")

    # get world matrix from xform_cache
    xform_cache = UsdGeom.XformCache(Usd.TimeCode.Default())
    world_mtx = get_world_transform_from_cache(xform_cache, cube)
    print(f"world matrix from cache: {world_mtx}")

    # show resulting USD content
    print(stage.GetRootLayer().ExportToString())


def set_TRS(prim, translate, rotate, scale, rotate_order=UsdGeom.XformCommonAPI.RotationOrderXYZ):
    xform_api = UsdGeom.XformCommonAPI(prim)
    xform_api.SetTranslate(translate)
    xform_api.SetRotate(rotate, rotate_order)
    xform_api.SetScale(scale)


def set_local_transform(prim, matrix: Gf.Matrix4d):
    xformable = UsdGeom.Xformable(prim)
    xformOp = xformable.AddTransformOp()
    xformOp.Set(matrix)


def get_bbox(prim):
    prim_img = UsdGeom.Imageable(prim)
    time = Usd.TimeCode.Default()
    prim_bound = prim_img.ComputeWorldBound(time, UsdGeom.Tokens.default_)
    return prim_bound


def get_bbox_from_cache(bbox_cache, prim):
    return bbox_cache.ComputeWorldBound(prim)


def get_local_transform(prim):
    xformable = UsdGeom.Xformable(prim)
    return xformable.GetLocalTransformation()


def get_world_transform(prim, time_code=0):
    xformable = UsdGeom.Xformable(prim)
    return xformable.ComputeLocalToWorldTransform(time_code)


def get_parent_matrix(prim, time_code=0):
    xformable = UsdGeom.Xformable(prim)
    return xformable.ComputeParentToWorldTransform(time_code)


def get_world_transform_from_cache(xform_cache, prim):
    return xform_cache.GetLocalToWorldTransform(prim.GetPrim())


def extract_trs(matrix: Gf.Matrix4d):
    translate = matrix.ExtractTranslation()
    rotate = matrix.ExtractRotation()
    rotate_mtx = matrix.ExtractRotationMatrix()
    scale = Gf.Vec3d(
        rotate_mtx.GetRow(0).GetLength(),
        rotate_mtx.GetRow(1).GetLength(),
        rotate_mtx.GetRow(2).GetLength()
    )
    return(translate, rotate, scale)

    
if __name__ == "__main__":
    main()
