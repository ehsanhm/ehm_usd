from pxr import Usd, UsdGeom


def main():
    stage = Usd.Stage.Open("reference.usda")
    
    # list all prims
    print(stage.Traverse())  # this is a PrimRange similar to iter in Python
    print(list(stage.Traverse()))

    # go through all prims with more detail
    prim_iter = iter(Usd.PrimRange.PreAndPostVisit(stage.GetPseudoRoot()))
    print('............')
    for x in prim_iter:
        if prim_iter.IsPostVisit():
            print(f"{x} [Post]")
        else:
            print(f"{x} [Pre]")
    
    # deactivate ref2
    ref2 = stage.GetPrimAtPath("/Ref2")
    ref2.SetActive(False)

    # deactivated prims will not be considered when traversing the scene
    print('............')
    for x in stage.Traverse():
        print(x)

    # traverse the deactivated prims too
    print('............')
    for x in stage.TraverseAll():
        print(x)


if __name__ == "__main__":
    main()
