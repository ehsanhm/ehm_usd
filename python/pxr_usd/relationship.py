from pxr import Usd, UsdGeom


def main():
    stage = Usd.Stage.CreateInMemory()

    cube = UsdGeom.Cube.Define(stage, "/World/Cube")
    cube_p = cube.GetPrim()

    for rel in cube_p.GetRelationships():
        print(rel)

    proxy_rel = UsdGeom.Imageable(cube).GetProxyPrimRel()
    proxy_rel.AddTarget("/World/Sphere")

    cone = UsdGeom.Cone.Define(stage, "/World/Cone")
    cone_p = cone.GetPrim()
    rel: Usd.Relationship = cone_p.CreateRelationship("Driver")
    rel.AddTarget(cube_p.GetPath())

    print(stage.GetRootLayer().ExportToString())


if __name__ == "__main__":
    main()
