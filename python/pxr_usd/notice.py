def simple_notice():
    import omni
    from pxr import Tf, Usd

    def my_func(notice: Usd.Notice, stage: Usd.Stage):
            for p in notice.GetChangedInfoOnlyPaths():
                if p.IsPrimPropertyPath():
                    print(f"{p} is a property and was changed.")

    stage = omni.usd.get_context().get_stage()

    notice = Tf.Notice.Register(
        Usd.Notice.ObjectsChanged,
        my_func,
        stage
    )

    del notice
