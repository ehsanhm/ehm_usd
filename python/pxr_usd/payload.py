from pxr import Usd


def main():
    stage = Usd.Stage.CreateInMemory()

    holder_p = stage.DefinePrim("/World/Payload_Holder")

    holder_pay = holder_p.GetPayloads()
    holder_pay.AddPayload(
        assetPath="cube_color.usda",
        primPath="/Cube"
    )

    print(stage.GetRootLayer().ExportToString())


if __name__ == "__main__":
    main()
