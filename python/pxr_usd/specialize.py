from pxr import Usd, UsdGeom, Gf, Sdf


def main():
    stage = Usd.Stage.CreateInMemory()

    UsdGeom.SetStageMetersPerUnit(stage, UsdGeom.LinearUnits.meters)

    animal = UsdGeom.Xform.Define(stage, "/World/Animal")
    horse = stage.DefinePrim("/World/Horse")

    horse_sps: Usd.Specializes = horse.GetSpecializes()

    horse_sps.AddSpecialize(animal.GetPath())

    horse_rot_a: Usd.Attribute = animal.GetPrim().CreateAttribute('xformOp:RotateXYZ', Sdf.ValueTypeNames.Float3, False)
    horse_rot_a.Set(Gf.Vec3f(180, 0, 0))

    print(stage.GetRootLayer().ExportToString())


if __name__ == "__main__":
    main()
