from pxr import Usd, Sdf


def main():
    stage = Usd.Stage.CreateInMemory()

    world_p = stage.DefinePrim("/World")

    points_a = world_p.CreateAttribute("points", Sdf.ValueTypeNames.Float3Array)

    points_a.Set([(0, 1, 2), (3, 4, 5)])

    print(stage.GetRootLayer().ExportToString())


if __name__ == "__main__":
    main()
