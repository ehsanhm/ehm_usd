from pxr import Usd


def main():
    stage = Usd.Stage.CreateInMemory()
    print(dir(stage))

    prim_to_del = stage.DefinePrim('/PrimToDelete')

    print(f'GetFramesPerSecond: {stage.GetFramesPerSecond()}')
    print(f'GetTimeCodesPerSecond: {stage.GetTimeCodesPerSecond()}')
    print(f'GetStartTimeCode: {stage.GetStartTimeCode()}')
    print(f'GetEndTimeCode: {stage.GetEndTimeCode()}')
    print(f'HasDefaultPrim: {stage.HasDefaultPrim()}')
    print(f'GetDefaultPrim: {stage.GetDefaultPrim()}')

    stage.RemovePrim(prim_to_del.GetPath())

    print(stage.GetRootLayer().ExportToString())


if __name__ == "__main__":
    main()
