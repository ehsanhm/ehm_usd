from pxr import Usd, UsdGeom


def main():
    stage = Usd.Stage.CreateNew("hello_world.usdc")

    UsdGeom.Xform.Define(stage, '/Hello')
    UsdGeom.Sphere.Define(stage, "/Hello/World")

    stage.DefinePrim("/Hello2", "Cube")

    stage.GetRootLayer().Save()


if __name__ == "__main__":
    main()
