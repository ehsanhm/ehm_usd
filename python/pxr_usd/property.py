from pxr import Usd, UsdGeom, Gf


def main():
    stage = Usd.Stage.Open("hello_world_2.usda")

    xform_p = stage.GetPrimAtPath("/Hello")
    sphere_p = stage.GetPrimAtPath("/Hello/World")

    print(xform_p)
    print(sphere_p)

    print(xform_p.GetPropertyNames())
    print(sphere_p.GetPropertyNames())

    radius = sphere_p.GetAttribute("radius").Get()
    rel = sphere_p.GetRelationship("radius")
    if rel.IsValid():
        print(f"rel: {rel}")
    else:
        print(f"radius is not a relationship")
    print(f"Radius: {radius}")

    print(sphere_p.GetAttribute("extent").Get())
    sphere_p.GetAttribute("radius").Set(1.25)
    print(sphere_p.GetAttribute("extent").Get())

    for prop in sphere_p.GetPropertyNames():
        rel = sphere_p.GetRelationship(prop)
        if rel.IsValid():
            print(f"{prop} is a relationship")

    for rel in sphere_p.GetRelationships():
        print(f"rel: {rel}")

    for prop in sphere_p.GetProperties():
        print(f"prop: {prop}")

    # print(dir(prop))

    extent_at = sphere_p.GetProperty("extent")
    print(extent_at.GetTypeName())

    extent_at = sphere_p.GetAttribute("extent")
    print(extent_at.GetTypeName())
    print(extent_at.GetTimeSamples())

    extent_at.Set(extent_at.Get() * 1.25)
    print(extent_at.Get())

    stage_str = stage.GetRootLayer().ExportToString()
    print(stage_str)
    # print(dir(stage))

    print(stage.GetStartTimeCode())
    print(stage.GetEndTimeCode())
    print(stage.GetFramesPerSecond())
    print(stage.GetAttributeAtPath("/Hello/World.radius"))

    # stage.RemovePrim("Hello/World")
    print([x for x in stage.Traverse()])
    print([x for x in stage.TraverseAll()])

    color_at = sphere_p.GetAttribute("primvars:displayColor")
    print(color_at.Get())
    color_at.Set([Gf.Vec3f(1, 0, 0)])

    if stage.HasDefaultPrim():
        print(f'default prims: {stage.GetDefaultPrim()}')
    else:
        print('no default prim')

    stage.SetDefaultPrim(xform_p)

    stage.Save()


if __name__ == "__main__":
    main()
