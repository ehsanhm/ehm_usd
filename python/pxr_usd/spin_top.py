from pxr import Usd, UsdGeom, Sdf


def main():
    create_spin_top_animation()
    
    stage = make_stage("spin_top_retime.usda")

    # reference the spinTop with default animation speed and time offset
    left = UsdGeom.Xform.Define(stage, "/Left")
    UsdGeom.XformCommonAPI(left).SetTranslate([-1.5, 0, 0])
    reference_file(
        stage=stage,
        prim_name="/Left/SpinTop",
        asset_path="spin_top_anim.usda",
        prim_path="/SpinTop"
    )

    # center spinTop should start spinning at frame 96
    reference_file(
        stage=stage,
        prim_name="/Center",
        asset_path="spin_top_anim.usda",
        prim_path="/SpinTop",
        offset=96
    )

    # right spinTop should turn twice as fast
    right = UsdGeom.Xform.Define(stage, "/Right")
    UsdGeom.XformCommonAPI(right).SetTranslate([1.5, 0, 0])
    reference_file(
        stage=stage,
        prim_name="/Right/SpinTop",
        asset_path="spin_top_anim.usda",
        prim_path="/SpinTop",
        scale=0.5
    )

    stage.Save()


def create_spin_top_animation():
    stage = make_stage("spin_top_anim.usda")

    spin_top = reference_file(
        stage,
        prim_name="/SpinTop",
        asset_path="./spin_top_geo.usda"
    )

    precession(spin_top)
    offset(spin_top)
    tilt(spin_top)
    spin(spin_top)

    # print(stage.GetRootLayer().ExportToString())
    stage.Save()


def make_stage(stage_path, start_time=1, end_time=200):
    stage = Usd.Stage.CreateNew(stage_path)
    UsdGeom.SetStageUpAxis(stage, UsdGeom.Tokens.z)
    stage.SetStartTimeCode(start_time)
    stage.SetEndTimeCode(end_time)
    stage.SetMetadata("comment", "Animated SpinTop")
    return stage


def reference_file(stage, prim_name, asset_path, prim_path=None, offset=0, scale=1):
    xform = UsdGeom.Xform.Define(stage, prim_name)

    # need to provide primPath if there's no defaultPrim in the file being referenced
    if prim_path:
        xform.GetPrim().GetReferences().AddReference(
            assetPath=asset_path,
            primPath=prim_path,  
            layerOffset=Sdf.LayerOffset(offset=offset, scale=scale)
        )
    else:
        xform.GetPrim().GetReferences().AddReference(
            assetPath=asset_path,
            layerOffset=Sdf.LayerOffset(offset=offset, scale=scale)
        )

    return xform    


def spin(prim):
    rot_a = prim.AddRotateZOp(opSuffix="spin")
    rot_a.Set(time=1, value=0)
    rot_a.Set(time=200, value=1440)


def tilt(prim):
    rot_a = prim.AddRotateXOp(opSuffix="tilt")
    rot_a.Set(value=12)


def precession(prim):
    rot_a = prim.AddRotateZOp(opSuffix="precession")
    rot_a.Set(time=1, value=0)
    rot_a.Set(time=200, value=360)


def offset(prim):
    translate_a = prim.AddTranslateOp(opSuffix="offset")
    translate_a.Set((0, 0.1, 0))


if __name__ == "__main__":
    main()
