from pxr import Usd, UsdGeom, Gf


def main():
    print(f'UsdGeom: {dir(UsdGeom)}')

    stage = Usd.Stage.CreateInMemory()

    cam = UsdGeom.Camera.Define(stage, "/World/MyCamera")
    xform = UsdGeom.Xform.Define(stage, "/World/MyXform")
    Cube = UsdGeom.Cube.Define(stage, "/World/Cube")

    print(f'GetStageMetersPerUnit: {UsdGeom.GetStageMetersPerUnit(stage)}')
    print(f'FallbackUpAxis: {UsdGeom.GetFallbackUpAxis()}')
    print(f'StageUpAxis: {UsdGeom.GetStageUpAxis(stage)}')

    UsdGeom.SetStageUpAxis(stage, UsdGeom.Tokens.z)

    # rotate the cube 45 degrees in X
    cube_xf = UsdGeom.Xformable(Cube)
    cube_rot_a = cube_xf.AddRotateXYZOp()
    cube_rot_a.Set(Gf.Vec3f(45, 0, 0))

    # cache the bounding boxes. this is more efficient if querying bbox multiple times
    purposes = [UsdGeom.Tokens.default_, UsdGeom.Tokens.proxy, UsdGeom.Tokens.render]
    bbox_cache = UsdGeom.BBoxCache(0, purposes)

    # get cube's aligned bounding box using BBoxCache
    cube_bounds = bbox_cache.ComputeWorldBound(Cube.GetPrim())
    print(cube_bounds.ComputeAlignedBox())

    # we can also get the bounding box using Imageable, which is more efficient if we want to query bbox once (I think)
    cube_imageable = UsdGeom.Imageable(Cube)
    cube_bounds = cube_imageable.ComputeWorldBound(0, *purposes)
    print(cube_bounds.ComputeAlignedBox())

    # print(f'Xformable: {dir(UsdGeom.Xformable)}')
    # print(f'UsdGeom.Tokens: {dir(UsdGeom.Tokens)}')
    # print(f'UsdGeom.BBoxCache: {dir(UsdGeom.BBoxCache)}')
    # print(f'UsdGeom.Gprim: {dir(UsdGeom.Gprim)}')
    # print(f'XformCache: {dir(UsdGeom.XformCache)}')
    # print(f'XformOp: {dir(UsdGeom.XformOp)}')

    print(stage.GetRootLayer().ExportToString())


if __name__ == "__main__":
    main()
