from pxr import Usd, UsdGeom, Kind, Sdf, UsdShade


def main():
    stage = Usd.Stage.CreateNew("model.usda")
    UsdGeom.SetStageUpAxis(stage, UsdGeom.Tokens.y)

    # create a model
    model_root = UsdGeom.Xform.Define(stage, "/MyModel")
    Usd.ModelAPI(model_root).SetKind(Kind.Tokens.component)

    # create material
    card_mat = UsdShade.Material.Define(stage, "/MyModel/CardMat")

    # create shader
    card_shd = UsdShade.Shader.Define(stage, "/MyModel/CardMat/CardShader")
    card_shd.CreateIdAttr("UsdPreviewSurface")
    card_shd_dif = card_shd.CreateInput("diffuseColor", Sdf.ValueTypeNames.Float3)
    # card_shd_dif.Set((1.0, 0.3, 0.0))
    card_shd.CreateInput("metallic", Sdf.ValueTypeNames.Float).Set(0.1)
    card_shd.CreateInput("roughness", Sdf.ValueTypeNames.Float).Set(0.4)

    # create uv reader
    uv_reader = UsdShade.Shader.Define(stage, "/MyModel/CardMat/UVReader")
    uv_reader.CreateIdAttr("UsdPrimvarReader_float2")
    uv_varname = uv_reader.CreateInput("varname", Sdf.ValueTypeNames.Token)
    uv_varname.Set("st")    
    # card_mat_uv_name = card_mat.CreateInput("uv_primvar_name", Sdf.ValueTypeNames.Token)
    # card_mat_uv_name.Set("st")
    # uv_varname.ConnectToSource(card_mat_uv_name)

    # create texture
    card_tex = UsdShade.Shader.Define(stage, "/MyModel/CardMat/CardTexture")
    card_tex.CreateIdAttr("UsdUVTexture")
    card_tex.CreateInput("file", Sdf.ValueTypeNames.Asset).Set("pixar_clouds.jpg")
    card_tex_st = card_tex.CreateInput("st", Sdf.ValueTypeNames.Float2)

    # connect uv_reader to texture
    card_tex_st.ConnectToSource(uv_reader.ConnectableAPI(), "result")

    # connect texture to shader
    card_tex_rgb = card_tex.CreateOutput("rgb", Sdf.ValueTypeNames.Float3)
    card_shd_dif.ConnectToSource(card_tex_rgb)
    # card_shd.CreateInput("diffuseColor", Sdf.ValueTypeNames.Float3).ConnectToSource(card_tex.ConnectableAPI(), "rgb")
    

    # connect shader to material
    card_mat.CreateSurfaceOutput().ConnectToSource(card_shd.ConnectableAPI(), "surface")
    
    # create a plane
    mesh = create_mesh(stage, "/MyModel/Card")

    # assign material to plane
    mesh.GetPrim().ApplyAPI(UsdShade.MaterialBindingAPI)
    UsdShade.MaterialBindingAPI(mesh).Bind(card_mat)

    print(stage.GetRootLayer().ExportToString())
    stage.Save()


def create_mesh(stage, prim_path):
    mesh = UsdGeom.Mesh.Define(stage, prim_path)
    mesh.CreatePointsAttr([(0, 0, 0), (1, 0, 0), (1, 0.4, 0), (0, 0.4, 0)])
    mesh.CreateFaceVertexCountsAttr([4])
    mesh.CreateFaceVertexIndicesAttr([0, 1, 2, 3])
    uv_a = UsdGeom.PrimvarsAPI(mesh).CreatePrimvar(
        "st",
        Sdf.ValueTypeNames.TexCoord2fArray,
        UsdGeom.Tokens.varying
    )
    uv_a.Set([(0, 0), (1, 0), (1, 1), (0, 1)])
    return mesh


if __name__ == "__main__":
    main()
