ehm.blendshape.ui
#############################

Example of Python only extension


.. toctree::
   :maxdepth: 1

   README
   CHANGELOG


.. automodule::"ehm.blendshape.ui"
    :platform: Windows-x86_64, Linux-x86_64
    :members:
    :undoc-members:
    :show-inheritance:
    :imported-members:
    :exclude-members: contextmanager
