import omni.ext
import omni.usd
import carb
import omni.ui as ui
from pxr import UsdSkel


# Functions and vars are available to other extension as usual in python: `example.python_ext.some_public_function(x)`
def some_public_function(x: int):
    print("[ehm.blendshape.ui] some_public_function was called with x: ", x)
    return x ** x


# Any class derived from `omni.ext.IExt` in top level module (defined in `python.modules` of `extension.toml`) will be
# instantiated when extension gets enabled and `on_startup(ext_id)` will be called. Later when extension gets disabled
# on_shutdown() is called.
class EhmBlendshapeUiExtension(omni.ext.IExt):
    # ext_id is current extension id. It can be used with extension manager to query additional information, like where
    # this extension is located on filesystem.
    def on_startup(self, ext_id):
        print("[ehm.blendshape.ui] ehm blendshape ui startup")
        self._context = omni.usd.get_context()
        # self._stage = self._context.get_stage()
        self._stage_event_sub = (
            self._context
            .get_stage_event_stream()
            .create_subscription_to_pop(self._build_ui, name="ehm.blendshape.ui stage event sub")
        )

        # self._build_ui()


    def _build_ui(self, event):
        if event.type == int(omni.usd.StageEventType.ASSETS_LOADED):
            self._stage = self._context.get_stage()
            carb.log_error(f"_stage reloaded")

        # if event.type == int(omni.usd.StageEventType.SELECTION_CHANGED):
        #     prim_paths = self._context.get_selection().get_selected_prim_paths()
        #     if not prim_paths:
        #         return
        #     prim = self._context.get_stage().GetPrimAtPath(prim_paths[-1])
        #     if prim.IsA(UsdSkel.Skeleton):
        #         carb.log_error(f"Skeleton: {prim}")

                # self._window = ui.Window("My Window", width=300, height=300)
                # with self._window.frame:
                #     with ui.VStack():
                #         label = ui.Label("aaaa")
                #         with ui.HStack():
                #             ui.Button("Add")
                #             # ui.Button("Reset", clicked_fn=on_reset)


    def on_shutdown(self):
        print("[ehm.blendshape.ui] ehm blendshape ui shutdown")
        self._stage_event_sub = None
        # self._app_event_sub = None
    