import os
import omni
import carb
import omni.ui as ui
from omni.kit.window.property.templates import SimplePropertyWidget
from omni.kit.property.usd.custom_layout_helper import CustomLayoutFrame, CustomLayoutProperty
from omni.kit.property.usd.usd_property_widget import UsdPropertiesWidget
from omni.kit.property.usd.usd_attribute_widget import UsdPropertyUiEntry
from omni.kit.property.usd.usd_property_widget import UsdPropertiesWidgetBuilder
from omni.kit.property.usd.relationship import RelationshipEditWidget

from pxr import Usd, Sdf, UsdSkel

# check C:\GIT\ehm_usd\python\omni_usd\kit-exts-project\app\kit\extsPhysics\omni.physx.vehicle-104.2.4-5.1\omni\physxvehicle\scripts\properties\propertyWidgets.py
# and C:\GIT\ehm_usd\python\omni_usd\kit-exts-project\app\extscache\omni.anim.shared-104.14.1+104.2.wx64.r.cp37\omni\anim\shared\scripts\autoPropertyWidget.py
# good icons: C:\GIT\ehm_usd\python\omni_usd\kit-exts-project\app\kit\exts\omni.kit.property.usd\data\icons

EXT_PATH = omni.kit.app.get_app().get_extension_manager().get_extension_path_by_module(__name__)
ICON_PATH = os.path.join(EXT_PATH, "data")


class MinimalItem(ui.AbstractItem):
    def __init__(self, text):
        super().__init__()
        self.model = ui.SimpleStringModel(text)


class MinimalModel(ui.AbstractItemModel):
    def __init__(self):
        super().__init__()

        # self._current_index = ui.SimpleIntModel()
        # self._current_index.add_value_changed_fn(
        #     lambda a: self._item_changed(None))

        self._items = [MinimalItem("Option 1")]


    def get_item_value_model(self, item, column_id):
        # if item is None:
        #     return self._current_index
        return item.model


class SkeletonPropertyWidget(SimplePropertyWidget):
    def __init__(self):
        super().__init__(title="Skeletal Animation 2", collapsed=False)
        carb.log_error("SkeletonPropertyWidget!")

    # def _customize_props_layout(self, props):

    #     # frame = ui.Frame()
    #     # with frame:
    #     #     with ui.HStack():
    #     #         ui.Label("No animation!", width=0)
    #     #         with ui.Frame():
    #     #             with ui.HStack():
    #     #                 ui.Label("BBBBBBBBBBBBB!", width=0)

    #     frame = CustomLayoutFrame(hide_extra=True)
    #     with frame:
    #         #     ui.Label("No animation!", width=0)
    #         CustomLayoutProperty("skel:animationSource", " ")
    #         # CustomLayoutProperty(None, None)
    #     return frame.apply(props)
    
    #     # frame = ui.Frame()
    #     # with frame:
    #     #     with ui.HStack(spacing=4):
    #     #         ui.Label("No animation!", width=0)
    #     #         RelationshipEditWidget(
    #     #             stage=self._stage,
    #     #             attr_name="skel:animationSource",
    #     #             prim_paths=[self._prim.GetPath()],
    #     #             # additional_widget_kwargs=self.get_additional_kwargs()
    #     #         )
    #     #         # a._build()
    #     # return frame.apply(props)

    # def _filter_props_to_build(self, props):
    #     return [prop for prop in props if prop.GetName() in ["skel:animationSource"]]

    def _on_target_prim_rel_changed(self):
        pass
    
    def on_new_payload(self, payload):
        carb.log_error("SkeletonPropertyWidget.on_new_payload")
        if not super().on_new_payload(payload):
            return False
        # show this widget only if the last selected prim is a Skeleton and has BindingAPI
        self._stage = payload.get_stage()
        paths = payload.get_paths()
        if self._stage and paths:
            self._prim = self._stage.GetPrimAtPath(paths[-1])
            if self._prim:
                if self._prim.IsA(UsdSkel.Skeleton):
                    carb.log_error(f"Found BindinAPI on prim {paths[-1]}!")
                    return True

    # def get_additional_kwargs(self, ui_attr):
    #     random_prim = Usd.Prim()
    #     prim_same_type = True
    #     if self._payload:
    #         prim_paths = self._payload.get_paths()
    #         for prim_path in prim_paths:
    #             prim = self._get_prim(prim_path)
    #             if not prim:
    #                 continue
    #             if not random_prim:
    #                 random_prim = prim
    #             elif random_prim.GetTypeName() != prim.GetTypeName():
    #                 prim_same_type = False
    #                 break
    #     if ui_attr.prop_name == "skel:animationSource" and prim_same_type and random_prim \
    #         and (random_prim.IsA(UsdSkel.Skeleton) or random_prim.IsA(UsdSkel.Root)):
    #         return None, {"target_picker_filter_type_list": [UsdSkel.Animation], "targets_limit": 1}
    #     return None, {"targets_limit": 0}

    def build_items(self):
        with ui.HStack(spacing=4):
            ui.Image(os.path.join(ICON_PATH, "icon.png"), width=100, height=100)

            with ui.VStack(spacing=4):
                # skeleton doesn't have BindingAPI, show "Add Skeletal Animation" button
                if not self._prim.HasAPI(UsdSkel.BindingAPI):
                    with ui.HStack(spacing=4):
                        ui.Label("No animation!", width=0)
                        # ui.Spacer()
                        ui.Button(text="Add Skeletal Animation", clicked_fn= self._add_skel_anim)

                # skeleton has skeL:animationSource, show path to anim
                elif self._prim.GetProperty("skel:animationSource"):
                    with ui.HStack(spacing=4):
                        # self._model = MinimalModel()
                        self._copy_btn = ui.Button(text="Copy", clicked_fn=self.copy_btn_clicked)
                        self._paste_btn = ui.Button(text="Paste", clicked_fn=self.paste_btn_clicked)
                        self._paste_btn.enabled = False
                        # anim_field = ui.StringField() #model=self._model)
                        self._reset_btn = ui.Button(text=" ", width=20, height=20, clicked_fn=self._reset_btn_clicked)
                        # CustomLayoutProperty("skel:animationSource", "Animation Source")
                        # carb.log_error(f".............., {anim_field.model.get_value_as_string()}")
                        # self.add_item_with_model(label="aaa", model=self._model)
    
    def copy_btn_clicked(self):
        carb.log_error("copied")
        self._paste_btn.enabled = True
        self._copy_btn.enabled = False

    def paste_btn_clicked(self):
        carb.log_error("pasted")
        self._copy_btn.enabled = True
        self._paste_btn.enabled = False

    def reset_btn_clicked(self):
        carb.log_error("reset")

    def _add_skel_anim(self):
        omni.kit.commands.execute("ApplySkelBindingAPICommand", paths=[self._prim.GetPath()])
        self.request_rebuild()
