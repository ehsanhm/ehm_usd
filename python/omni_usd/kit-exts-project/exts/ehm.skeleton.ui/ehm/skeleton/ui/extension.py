import omni.ext
import omni.usd
import carb
import omni.ui as ui
from pxr import UsdSkel
from .property_widget import SkeletonPropertyWidget


class EhmSkeletonUiExtension(omni.ext.IExt):
    def on_startup(self, ext_id):
        print("[ehm.skeleton.ui] ehm skeleton ui startup")
        import omni.kit.window.property as p
        w = p.get_window()
        if w:
            w.register_widget("prim", "anim_graph_skeleton2", SkeletonPropertyWidget())


    def on_shutdown(self):
        print("[ehm.skeleton.ui] ehm skeleton ui shutdown")
        import omni.kit.window.property as p
        w = p.get_window()
        if w:
            w.unregister_widget("prim", "anim_graph_skeleton2")
