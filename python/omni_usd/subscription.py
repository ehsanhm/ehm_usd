import omni
import carb


# event types found in omni.usd.StageEventType
STAGE_EVENT_TYPES_DICT = {
    'SAVED': 0,
    'SAVE_FAILED': 1,
    'OPENING': 2,
    'OPENED': 3,
    'OPEN_FAILED': 4,
    'CLOSING': 5,
    'CLOSED': 6,
    'SELECTION_CHANGED': 7,
    'ASSETS_LOADED': 8,
    'ASSETS_LOAD_ABORTED': 9,
    'GIZMO_TRACKING_CHANGED': 10,
    'MDL_PARAM_LOADED': 11,
    'SETTINGS_LOADED': 12,
    'SETTINGS_SAVING': 13,
    'OMNIGRAPH_START_PLAY': 14,
    'OMNIGRAPH_STOP_PLAY': 15,
    'SIMULATION_START_PLAY': 16,
    'SIMULATION_STOP_PLAY': 17,
    'ANIMATION_START_PLAY': 18,
    'ANIMATION_STOP_PLAY': 19,
    'DIRTY_STATE_CHANGED': 20,
    'ASSETS_LOADING': 21,
    'HYDRA_GEOSTREAMING_STARTED': 22,
    'HYDRA_GEOSTREAMING_STOPPED': 23,
    'HYDRA_GEOSTREAMING_STOPPED_NOT_ENOUGH_MEM': 24,
    'HYDRA_GEOSTREAMING_STOPPED_AT_LIMIT': 25,
}


class Subscribe_to_stage_event:

    def on_startup(self):
        self._stage_event_sub = (
            omni.usd.get_context()
            .get_stage_event_stream()
            .create_subscription_to_pop(self.some_func, name="some name")
        )

    def on_shutdown(self):
        self._stage_event_sub = None

    def some_func(self, event):
        if event.type == int(omni.usd.StageEventType.SELECTION_CHANGED):
            print("SUBSCRIBE_TO_STAGE_EVENT worked. Selection was changed!")


class Subscribe_to_app_update:

    def on_startup(self):
        self._app_event_sub = (
            omni.kit.app.get_app()
            .get_update_event_stream()
            .create_subscription_to_pop(self._app_update_fn, order=1000000)
        )

    def on_shutdown(self):
        self._app_event_sub = None


    def _app_update_fn(self, event):
        print("SUBSCRIBE_TO_APP_UPDATE worked (this runs every app update... careful!)")
        self._time += event.payload["dt"]
        if self._time > self._counter:
            self._counter += 1
            carb.log_warn(f"Time elapsed: {round(self._time)}")


class Subscribe_to_timeline_event:    

    def on_startup(self):
        if self._timeline_event_sub is not None:
            return
        timeline_events = omni.timeline.get_timeline_event_stream()
        self._timeline_event_sub = timeline_events.create_subscription_to_pop(self._on_timeline_event)
        
    def _on_timeline_event(self, event):
        if event.type == int(omni.timeline.TimelineEventType.CURRENT_TIME_TICKED) \
        or event.type == int(omni.timeline.TimelineEventType.CURRENT_TIME_CHANGED):
            current_time = event.payload["currentTime"]            
            carb.log_warn(f"Current time: {round(current_time)}")

    def on_shutdown(self):
        self._timeline_event_sub = None
