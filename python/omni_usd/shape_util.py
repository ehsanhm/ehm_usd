import omni.usd

from pxr import UsdSkel, UsdGeom


ARKIT_SHAPES = ['eyeBlinkLeft', 'eyeLookDownLeft', 'eyeLookInLeft', 'eyeLookOutLeft', 'eyeLookUpLeft', 'eyeSquintLeft', 'eyeWideLeft', 'eyeBlinkRight', 'eyeLookDownRight', 'eyeLookInRight', 'eyeLookOutRight', 'eyeLookUpRight', 'eyeSquintRight', 'eyeWideRight', 'jawForward', 'jawLeft', 'jawRight', 'jawOpen', 'mouthClose', 'mouthFunnel', 'mouthPucker', 'mouthLeft', 'mouthRight', 'mouthSmileLeft', 'mouthSmileRight', 'mouthFrownLeft', 'mouthFrownRight', 'mouthDimpleLeft', 'mouthDimpleRight', 'mouthStretchLeft', 'mouthStretchRight', 'mouthRollLower', 'mouthRollUpper', 'mouthShrugLower', 'mouthShrugUpper', 'mouthPressLeft', 'mouthPressRight', 'mouthLowerDownLeft', 'mouthLowerDownRight', 'mouthUpperUpLeft', 'mouthUpperUpRight', 'browDownLeft', 'browDownRight', 'browInnerUp', 'browOuterUpLeft', 'browOuterUpRight', 'cheekPuff', 'cheekSquintLeft', 'cheekSquintRight', 'noseSneerLeft', 'noseSneerRight', 'tongueOut']


def get_selected_prims():
    stage = omni.usd.get_context().get_stage()
    sels = omni.usd.get_context().get_selection()
    prim_paths = sels.get_selected_prim_paths()
    prims = [stage.GetPrimAtPath(x) for x in prim_paths]
    return prims


def get_blendshape_names() -> list:
    """ returns list of blendShape targets of selected prim """
    for prim in get_selected_prims():

        if prim.IsA(UsdSkel.Animation):
            bs_prop = prim.GetAttribute("blendShapes")
            if bs_prop:
                return bs_prop.Get()

        elif prim.IsA(UsdGeom.Mesh):
            bs_prop = prim.GetAttribute("skel:blendShapes")
            if bs_prop:
                return bs_prop.Get()


def get_blendshape_weights() -> list:
    """ returns blendshape weights of selected prim """
    for prim in get_selected_prims():
        if prim.IsA(UsdSkel.Animation):
            bs_prop = prim.GetAttribute("blendShapeWeights")
            if bs_prop:
                bs_wgts = bs_prop.Get()
                return bs_wgts


def get_blendshape_timesamples() -> list:
    """ returns blendshape timesamples of selected prim """
    for prim in get_selected_prims():
        if prim.IsA(UsdSkel.Animation):
            bs_prop = prim.GetAttribute("blendShapeWeights")
            if bs_prop:
                return bs_prop.GetTimeSamples()


def clear_blendshape_weights():
    """ removes blendshape weight values and timesamples of selected prim """
    for prim in get_selected_prims():
        if prim.IsA(UsdSkel.Animation):
            bs_prop = prim.GetAttribute("blendShapeWeights")
            if bs_prop:
                bs_timesamples = bs_prop.Clear()


def set_blendshape_names(shape_list) :
    for prim in get_selected_prims():

        if prim.IsA(UsdSkel.Animation):
            bs_prop = prim.GetAttribute("blendShapes")
            if bs_prop:
                bs_prop.Clear()
                bs_prop.Set(shape_list)
            bs_wgts_prop = prim.GetAttribute("blendShapeWeights")
            if bs_wgts_prop:
                bs_wgts_prop.Clear()
                bs_wgts_prop.Set([0.0] * len(shape_list))

        elif prim.IsA(UsdGeom.Mesh):
            bs_prop = prim.GetAttribute("skel:blendShapes")
            if bs_prop:
                bs_prop.Clear()
                bs_prop.Set(shape_list)


def remove_trailing_numbers_from_shapes():
    for prim in get_selected_prims():
        if prim.IsA(UsdSkel.Animation):
            bs_prop = prim.GetAttribute("blendShapes")
            if bs_prop:
                new_bs = []
                for x in bs_prop.Get():

                    # remove numbers
                    while x[-1].isdigit():
                        if len(x) < 2:
                            x = "Invalid_Name"
                            break
                        x = x [:-1]

                    new_bs.append(x)
                bs_prop.Set(new_bs)
