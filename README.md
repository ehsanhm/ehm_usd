# ehm_usd

Description: Pixar USD and NVIDIA example codes in Python and CPP

Authors: ["Ehsan Hassani Moghaddam"]

Git Adress: https://gitlab.com/ehsanhm/ehm_usd

Quick usage sample:
```python
import os, sys, importlib
path = "C:/GIT/ehm_usd/python"
while path in sys.path:
    sys.path.remove(path)
sys.path.insert(0, path)

from pxr_usd import transform
importlib.reload(transform)
transform.main()
```